# We need to make sure that python knows about traci
# If you have trouble importing traci, you may need to edit the path in `lib/import_sumo.py`
from lib import import_sumo

# import traci
import traci
import traci.constants as tc

# other dependencies
from time import sleep, time

# Assumes `sumo-gui` is on the path already.  If not, you will need to update it to include the fully qualified path to your sumo-gui binary
sumoBinary = "sumo-gui"
sumoCmd = [sumoBinary, "-c", "simple.sumocfg",
           "--start", "--time-to-teleport", "100000000"]

# Start the simulation
traci.start(sumoCmd)
step = 0

#step-3.H
traci.vehicle.setStop("veh1", "main_3_wb", duration=30)
#step-3.F
fuel_consumption = 0
# Begin the simulation.  We set the step time to a high number (10000 seconds) to keep it running for a while
while step < 10000:

    # Get the current epoch time
    t = int(time())

    # Simulate the SUMO step
    traci.simulationStep()

    # increase the step for the next iteration
    step += 1

    # slow the simulation down so we can watch it
    sleep(0.2)

    print("stp #" + str(step))

    # fuel consumption
    fuel_consumption += (traci.vehicle.getFuelConsumption("veh1"))
    if traci.vehicle.getRoadID("veh1") == "x1_s_sb":
        speed = traci.vehicle.getSpeed("veh1")
        break
    print(step)

MPG_value = (speed/(fuel_consumption))*3785.41
print("MPG:", MPG_value)

    # Below are some helpful functions that might come in handy

    # speed = traci.vehicle.getSpeed("vehicleId") # m/s
    # current_road = traci.vehicle.getRoadID("vehicleId") # current edge name
    # fuel_consumption = traci.vehicle.getFuelConsumption("vehicleId")  # ml/s

    # For other available functions on `traci.vehicle`, check out https://sumo.dlr.de/pydoc/traci._vehicle.html